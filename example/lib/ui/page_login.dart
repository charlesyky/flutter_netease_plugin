import 'package:flutter/material.dart';
import 'package:flutter_netease_plugin/flutter_netease_plugin.dart';
import 'package:flutter_netease_plugin_example/utils/user_utils.dart';
import 'package:flutter_netease_plugin_example/zeus_kit/zeus_kit.dart';

class LoginHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LoginPage(),
    );
  }
}

/// 登录页面
class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _accountEditingController = TextEditingController();
  TextEditingController _passwordEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("登录"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            TextField(
              controller: _accountEditingController,
              decoration: InputDecoration(hintText: "请输入账号"),
            ),
            TextField(
              controller: _passwordEditingController,
              decoration: InputDecoration(hintText: "请输入密码"),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "请使用您在云信Demo中注册的账号密码",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 12.0,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            RaisedButton(
              child: Text("登录"),
              onPressed: () {
                _login();
              },
            ),
          ],
        ),
      ),
    );
  }

  void _login() async {
    final imAccount = _accountEditingController.text;
    final imToken = ZKCommonUtils.generateMd5(_passwordEditingController.text);

    bool isLoginSuccess = await FlutterNeteasePlugin().login(imAccount, imToken);

    if (isLoginSuccess) {
      UserUtils.saveIMLoginInfo(imAccount, imToken);
      // ZKRouter.pushWidget(context, MyApp(), replaceCurrent: true);
      ZKCommonUtils.showToast("login success");
    } else {
      ZKCommonUtils.showToast("login failed");
    }
  }
}
