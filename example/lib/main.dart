import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_netease_plugin/flutter_netease_plugin.dart';
import 'package:flutter_netease_plugin_example/utils/user_utils.dart';
import 'package:flutter_netease_plugin_example/zeus_kit/zeus_kit.dart';
import 'package:flutter_netease_plugin_example/ui/page_login.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();

  final imAccount = 'test1234';
  final imToken = ZKCommonUtils.generateMd5('test1234');

  // TODO appkey
  FlutterNeteasePlugin().init(
    appKey: "asdfg",
    // apnsCername: "ENTERPRISE",
    // apnsCernameDevelop: "DEVELOPER",
    imAccount: imAccount,
    imToken: imToken,
  );

  // bool isLogin = await UserUtils.isLogin();
  bool isLogin = false;
  if (isLogin) {
    runApp(MyApp());
  } else {
    runApp(LoginHomePage());
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await FlutterNeteasePlugin().platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Text('Running on: $_platformVersion\n'),
        ),
      ),
    );
  }
}
